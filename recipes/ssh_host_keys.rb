#
# Copyright:: Copyright (c) 2017 GitLab Inc.
# License:: MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

# Note: This recipe will overwrite SSH host keys with values from the same
# encrypted data bag used by the omnibus-gitlab::default recipe.  Use with
# care! If you supply invalid host keys, you may loose SSH access to your
# server.

attributes_with_secrets = merge_secrets('omnibus-gitlab')
ssh = attributes_with_secrets['ssh']

ssh['host_keys'].each do |filename, key_material|
  key_path = "/etc/ssh/#{filename}"

  # The script below first tries to generate a public key from the supplied
  # private key. If this fails, the Chef run will fail and the target file is
  # not overwritten.
  bash "install SSH host key #{filename}" do
    code %{
set -e
set -u

temp_suffix=.install-ssh-key.$(date +%s).$$
temp_key=#{key_path}${temp_suffix}

mkdir -p /etc/ssh

umask 077
cat > ${temp_key} <<EOF
#{key_material}
EOF

# The next step will fail or time out if the key material is invalid
ssh-keygen -y -f ${temp_key} > ${temp_key}.pub

# The key looks legit, move it into place
chmod 644 ${temp_key}.pub
mv ${temp_key} #{key_path}
mv ${temp_key}.pub #{key_path}.pub


# If this script timed out, you probably provided an invalid SSH host key!
    }
    # This timeout is crucial. If the user supplies an invalid SSH host key
    # then ssh-keygen will prompt the user for a password, effectively making
    # the script hang.
    timeout 10
    not_if { File.exist?(key_path) && File.read(key_path).strip == key_material.strip }
  end
end
